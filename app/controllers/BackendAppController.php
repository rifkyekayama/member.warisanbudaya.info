<?php

class BackendAppController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$jumlahBudaya = Budaya::where('user_id','=',Sentry::getUser()->id)->count();
		$data = [
			'jumlahBudaya'	=> $jumlahBudaya,
		];
		return View::make('backend.pages.dashboard', $data);
	}

	public function register()
	{
		return View::make('backend.pages.auth.register');
	}

	public function doRegister()
	{
		$rules = [
			'nama_depan' 			=> 'required|alpha',
			'nama_belakang' 		=> 'required|alpha',
			'email' 				=> 'required|email',
			'password' 				=> 'required|min:8',
			'konfirmasi_password' 	=> 'required|same:password',
			'setuju'				=> 'required',
		];

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			return Redirect::back()->withInput()->withErrors($validator);
		}else{
			try
			{
				// Let's register a user.
				$user = Sentry::register(array(
					'first_name'	=> Input::get('nama_depan'),
					'last_name'		=> Input::get('nama_belakang'), 
					'email'    		=> Input::get('email'),
					'password' 		=> Input::get('password'),
				));

				// Let's get the activation code
				$activationCode = $user->getActivationCode();

				// Send activation code to the user so he can activate the account
				Mail::send('backend.pages.auth.activationMail', array('nama' => Input::get('nama_depan')." ".Input::get('nama_belakang'), 'token' => $activationCode), function($message){
					$message->to(Input::get('email'))->subject('Aktivasi Pengguna');
				});

				Session::flash('success', 'Aktivasi User berhasil dikirim');
				return Redirect::to('login');
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
				Session::flash('error', 'Bidang isian email wajib diisi.');
				return Redirect::back()->withInput();
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
				Session::flash('error', 'Bidang isian password wajib diisi.');
				return Redirect::back()->withInput();
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
				Session::flash('error', 'Pengguna sudah terdaftar.');
				return Redirect::back()->withInput();
			}
		}
	}

	public function activationMember($token)
	{
		try
		{
			// Find the user using the user id
			$user = Sentry::findUserById(User::where('activation_code','=',$token)->get()[0]->id);

			// Attempt to activate the user
			if ($user->attemptActivation($token))
			{
				// User activation passed
				Session::flash('success', 'Pengguna berhasil diaktivasi');
				return Redirect::to('login');
			}
			else
			{
				// User activation failed
				Session::flash('error', 'Pengguna gagal diaktivasi');
				return Redirect::to('login');
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			Session::flash('error', 'Pengguna tidak ditemukan');
			return Redirect::to('login');
		}
		catch (Cartalyst\Sentry\Users\UserAlreadyActivatedException $e)
		{
			Session::flash('error', 'Pengguna sudah diaktivasi sebelumnya');
			return Redirect::to('login');
		}
	}

	public function login()
	{
		//
		return View::make('backend.pages.auth.login');
	}


	public function auth()
	{
		//
		$rules = array(
			'email'    => 'required|email',
			'password' => 'required|alphaNum|min:5'
		);
 
		$validator = Validator::make(Input::all(), $rules);
	 
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator);
		} else {
			try
			{
				$credentials = array(
					'email'     => Input::get('email'),
					'password'  => Input::get('password')
				);
				$remember = false;
				$remember = Input::get('remember');
				// digunakan untuk login
				$user = Sentry::authenticate($credentials, $remember);
				if($user){
					$customer = Sentry::findUserByLogin(Input::get('email'));
					return Redirect::to('/')->with('success','login berhasil');
				}
	 
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
				Session::flash('error', 'Login field is required.');
				return Redirect::to('login')
					->withInput(Input::except('password'))
					->with('error','Login field is required.');
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
				Session::flash('error', 'Password field is required.');
				return Redirect::to('login')
					->withInput(Input::except('password'))
					->with('error','Password field is required.');
			}
			catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
			{
				Session::flash('error', 'Wrong password, try again.');
				return Redirect::to('login')
					->withInput(Input::except('password'))
					->with('error','Wrong password, try again.');
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				Session::flash('error', 'Pengguna tidak ditemukan.');
				return Redirect::to('login');
			}
			catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
			{
				Session::flash('error', 'Pengguna belum diaktivasi.');
				return Redirect::to('login');
			}
	 
			// The following is only required if the throttling is enabled
			catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
			{
				Session::flash('error', 'User is suspended.');
				return Redirect::to('login')
					->withInput(Input::except('password'))
					->with('error','User is suspended.');
			}
			catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
			{
				Session::flash('error', 'User is banned.');
				return Redirect::to('login')
					->withInput(Input::except('password'))
					->with('error','User is banned.');
			}
		}
	}
	public function logout(){
		Sentry::logout();
		return Redirect::to('login')->with('success','Logout Berhasil');
	}
}