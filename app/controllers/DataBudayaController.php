<?php

class DataBudayaController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$budaya = Budaya::where('user_id','=',Sentry::getUser()->id)->orderBy('id', 'desc')->get();
		//$budaya = Budaya::orderBy('id', 'desc')->get();
		$budaya = ['budaya' => $budaya];
		return View::make('backend.pages.budaya.index', $budaya);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$lokasi = Lokasi::where('lokasi_kabupatenkota','=',0)->where('lokasi_kecamatan','=',0)->where('lokasi_kelurahan','=',0)->orderBy('lokasi_nama','ASC')->lists('lokasi_nama','lokasi_propinsi');
		$data = [
			'lokasi' => $lokasi,
		];
		return View::make('backend.pages.budaya.create', $data);
	}

	public function getKabupaten($provinsi){
		$kabupaten = Lokasi::where('lokasi_propinsi','=',$provinsi)->where('lokasi_kecamatan','=',0)->where('lokasi_kelurahan','=',0)->where('lokasi_kabupatenkota','!=',0)->orderBy('lokasi_nama','ASC')->lists('lokasi_nama','lokasi_kabupatenkota');
		return Response::json(Form::select('kabupaten', array('default' => 'Pilih Kabupaten') + $kabupaten, 'default', array('class' => 'form-control', 'id' => 'kabupaten')));
	}

	public function getKecamatan($provinsi, $kabupaten){
		$kecamatan = Lokasi::where('lokasi_propinsi','=',$provinsi)->where('lokasi_kecamatan','!=',0)->where('lokasi_kelurahan','=',0)->where('lokasi_kabupatenkota','=',$kabupaten)->orderBy('lokasi_nama','ASC')->lists('lokasi_nama','lokasi_kecamatan');
		return Response::json(Form::select('kecamatan', array('default' => 'Pilih Kecamatan') + $kecamatan, 'default', array('class' => 'form-control', 'id' => 'kecamatan')));
	}

	public function getKelurahan($provinsi, $kabupaten, $kecamatan){
		$kelurahan = Lokasi::where('lokasi_propinsi','=',$provinsi)->where('lokasi_kecamatan','=',$kecamatan)->where('lokasi_kelurahan','!=',0)->where('lokasi_kabupatenkota','=',$kabupaten)->orderBy('lokasi_nama','ASC')->lists('lokasi_nama','lokasi_kode');
		return Response::json(Form::select('kelurahan', array('default' => 'Pilih Kelurahan') + $kelurahan, 'default', array('class' => 'form-control', 'id' => 'kelurahan')));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$validator = Validator::make(Input::all(),Budaya::$rules);
		if($validator->fails()){
			return Redirect::back()->withInput()->withErrors($validator);
		}else{
			$budaya = new Budaya;
			$budaya->Nama_Karya_Budaya = Input::get('Nama_Karya_Budaya');
			$budaya->Kondisi_Karya_Budaya_Saat_Ini = Input::get('Kondisi_Karya_Budaya_Saat_Ini');
			$budaya->provinsi = Input::get('provinsi');
			$budaya->kabupaten = Input::get('kabupaten');
			$budaya->kecamatan = Input::get('kecamatan');
			$budaya->kelurahan = Input::get('kelurahan');
			$budaya->pos = Input::get('pos');
			$budaya->Deskripsi_Karya_Budaya = Input::get('Deskripsi_Karya_Budaya');
			$budaya->Pelaku_Karya_Budaya = Input::get('Pelaku_Karya_Budaya');
			$budaya->nama = Input::get('nama');
			$budaya->alamat = Input::get('alamat');
			$budaya->telp = Input::get('telp');
			$budaya->email = Input::get('email');
			$budaya->waktu = Input::get('waktu');
			$budaya->tanggal = date("Y/m/d", strtotime(Input::get('tanggal')));
			$budaya->user_id = Sentry::getUser()->id;

			$dataGambar = array();
			$gambar = Input::file('Foto_Karya_Budaya');
			if(Input::hasFile('Foto_Karya_Budaya')){
				for($i=0;$i<count($gambar);$i++){
					$ext = $gambar[$i]->getMimeType();
					$size = $gambar[$i]->getSize();
					if($ext == 'image/jpeg' || $ext == 'image/png' || $ext == "image/gif" || $ext == "image/jpg"){
						if((($size/1024)/1024) <= 2){
							$destination = public_path().'/assets/backend/image/budaya/';

							$filename = str_replace(" ", "+", $gambar[$i]->getClientOriginalName()).rand(1,50).".".$gambar[$i]->getClientOriginalExtension();
							Image::make($gambar[$i]->getRealPath())->save($destination.$filename);
							array_push($dataGambar, $destination.$filename);
						}
					}
				}
			}

			$budaya->foto_terbaru = json_encode($dataGambar);
			$budaya->deskripsi_foto = Input::get('deskripsi_foto');
			$budaya->save();

			Session::flash('success', 'Data Budaya berhasil ditambahkan...');
			return Redirect::route('budaya.index');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$budaya = Budaya::find(Crypt::decrypt($id));
		$provinsi = Lokasi::where('lokasi_kabupatenkota','=',0)->where('lokasi_kecamatan','=',0)->where('lokasi_kelurahan','=',0)->orderBy('lokasi_nama','ASC')->lists('lokasi_nama','lokasi_propinsi');
		$kabupaten = Lokasi::where('lokasi_propinsi','=',$budaya->provinsi)->where('lokasi_kecamatan','=',0)->where('lokasi_kelurahan','=',0)->where('lokasi_kabupatenkota','!=',0)->orderBy('lokasi_nama','ASC')->lists('lokasi_nama','lokasi_kabupatenkota');
		$kecamatan = Lokasi::where('lokasi_propinsi','=',$budaya->provinsi)->where('lokasi_kecamatan','!=',0)->where('lokasi_kelurahan','=',0)->where('lokasi_kabupatenkota','=',$budaya->kabupaten)->orderBy('lokasi_nama','ASC')->lists('lokasi_nama','lokasi_kecamatan');
		$kelurahan = Lokasi::where('lokasi_propinsi','=',$budaya->provinsi)->where('lokasi_kecamatan','=',$budaya->kecamatan)->where('lokasi_kelurahan','!=',0)->where('lokasi_kabupatenkota','=',$budaya->kabupaten)->orderBy('lokasi_nama','ASC')->lists('lokasi_nama','lokasi_kode');
		$data = [
			'budaya' 	=> $budaya,
			'provinsi'	=> $provinsi,
			'kabupaten'	=> $kabupaten,
			'kecamatan'	=> $kecamatan,
			'kelurahan'	=> $kelurahan,
		];
		return View::make('backend.pages.budaya.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$validator = Validator::make(Input::all(),Budaya::$rules);
		if($validator->fails()){
			return Redirect::back()->withInput()->withErrors($validator);
		}else{
			$budaya = Budaya::find(Crypt::decrypt($id));
			$budaya->Nama_Karya_Budaya = Input::get('Nama_Karya_Budaya');
			$budaya->Kondisi_Karya_Budaya_Saat_Ini = Input::get('Kondisi_Karya_Budaya_Saat_Ini');
			$budaya->provinsi = Input::get('provinsi');
			$budaya->kabupaten = Input::get('kabupaten');
			$budaya->kecamatan = Input::get('kecamatan');
			$budaya->kelurahan = Input::get('kelurahan');
			$budaya->pos = Input::get('pos');
			$budaya->Deskripsi_Karya_Budaya = Input::get('Deskripsi_Karya_Budaya');
			$budaya->Pelaku_Karya_Budaya = Input::get('Pelaku_Karya_Budaya');
			$budaya->nama = Input::get('nama');
			$budaya->alamat = Input::get('alamat');
			$budaya->telp = Input::get('telp');
			$budaya->email = Input::get('email');
			$budaya->waktu = Input::get('waktu');
			$budaya->tanggal = date("Y/m/d", strtotime(Input::get('tanggal')));
			$budaya->user_id = Sentry::getUser()->id;

			$dataGambar = array();
			$gambar = Input::file('Foto_Karya_Budaya');
			if(Input::hasFile('Foto_Karya_Budaya')){
				for($i=0;$i<count($gambar);$i++){
					$ext = $gambar[$i]->getMimeType();
					$size = $gambar[$i]->getSize();
					if($ext == 'image/jpeg' || $ext == 'image/png' || $ext == "image/gif" || $ext == "image/jpg"){
						if((($size/1024)/1024) <= 2){
							$destination = public_path().'/assets/backend/image/budaya/';

							$filename = str_replace(" ", "+", $gambar[$i]->getClientOriginalName()).rand(1,50).".".$gambar[$i]->getClientOriginalExtension();
							Image::make($gambar[$i]->getRealPath())->save($destination.$filename);
							array_push($dataGambar, $destination.$filename);
						}
					}
				}
			}

			$budaya->foto_terbaru = json_encode($dataGambar);
			$budaya->deskripsi_foto = Input::get('deskripsi_foto');
			$budaya->save();

			Session::flash('success', 'Data Budaya berhasil diubah...');
			return Redirect::route('budaya.index');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		if(Request::ajax()){
			$budaya = Budaya::find(Crypt::decrypt($id));
			$budaya->delete();
			$budaya = Budaya::orderBy('id', 'desc')->get();
			return Response::json(View::make('backend.pages.budaya.element._tableBudaya', array('budaya' => $budaya))->render());
		}
	}

	public function delete($id)
	{
		$budaya = Budaya::find(Crypt::decrypt($id));
		$budaya->delete();
		Session::flash('success', 'Data Budaya berhasil dihapus...');
		return Redirect::back();
	}


}
