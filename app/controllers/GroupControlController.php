<?php

class GroupControlController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$group = Group::all();
		$group = ['group' => $group];
		return View::make('backend.pages.usercontrol.group.index', $group);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('backend.pages.usercontrol.group.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$validator = Validator::make(Input::all(), Group::$rules);
	 
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		} else {			 
			try
			{
				$group = Sentry::createGroup(array(
					'name'        => Input::get('name'),
					'permissions' => Input::get('cb'),
				));
			}
			catch (Cartalyst\Sentry\Groups\NameRequiredException $e)
			{
				Session::flash('error', 'Name field is required');
				return Redirect::to('user-control/group');
			}
			catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
			{
				Session::flash('error', 'Group already exists');
				return Redirect::to('user-control/group');
			}
	 
		Session::flash('success', 'Data Berhasil Ditambahkan');
		return Redirect::to('user-control/group');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$groupbyid = Group::findOrFail(Crypt::decrypt($id));
		$groupbyid =
		[
			'groupbyid' => $groupbyid
		];
		return View::make('backend.pages.usercontrol.group.edit', $groupbyid);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$rules = array(
			'name' => 'required',
		);
	 
		$validator = Validator::make(Input::all(), $rules);
	 
		if ($validator->fails()) {  
			return Redirect::back()->withErrors($validator)->withInput();
		} else {
			if(count(Input::get('cb')) > 0){
				try
				{
				$group = Sentry::findGroupById(Crypt::decrypt($id));
				$arrexs = array();
				foreach ($group->permissions as $key => $value) {
					if (array_key_exists($key, Input::get('cb'))) {
							$arrexs[$key] = '1';
					}
				}
				$arrexs2 = array();
				foreach ($group->permissions as $key => $value) {
					if (!array_key_exists($key, $arrexs)) {
							$arrexs2[$key] = '0';
					}
				}
				$arrexs3 = array_merge($arrexs2,Input::get('cb'));
				$group->name = Input::get('name');
				$group->permissions = $arrexs3;
				$group->permissions = array();
		 
				if ($group->save())
				{
					Session::flash('success', 'Data Berhasil Diubah');
					return Redirect::to('user-control/group');  
				}
				else
				{
					Session::flash('error', 'Data Gagal Diubah');
					return Redirect::to('user-control/group');
				}
				}
				catch (Cartalyst\Sentry\Groups\NameRequiredException $e)
				{
					Session::flash('error', 'Name field is required');
					return Redirect::to('user-control/group');
				}
				catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
				{
					Session::flash('error', 'Group already exists');
					return Redirect::to('user-control/group');
				}
				catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
				{
					Session::flash('error', 'Group was not found.');
					return Redirect::to('user-control/group');
				}
			}else{
				return Redirect::back();
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function delete($id){
		try
		{
			$group = Sentry::findGroupById(Crypt::decrypt($id));
			$group->delete();
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			Session::flash('error', 'Group was not found');
			return Redirect::to('user-control/group'); 
		}
	 
		Session::flash('success', 'Data Berhasil Dihapus');
		return Redirect::to('user-control/group');
	}


}
