<?php

class UserControlController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$user = User::orderBy('id', 'desc')->get();
		$user = ['user' => $user];
		return View::make('backend.pages.usercontrol.user.index', $user);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$group = Group::lists('name', 'id');
		$group = ['group' => $group];
		return View::make('backend.pages.usercontrol.user.create', $group);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$rules = array(
			'namaDepan' => 'required',
			'namaBelakang' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|between:4,11',
			'confirmPassword' => 'between:4,11|same:password',
			'group' => 'required',
		);
	 
		$validator = Validator::make(Input::all(), $rules);
	 
		if ($validator->fails()) {  
			return Redirect::back()->withErrors($validator)->withInput();
		} else {           
			 
			try
			{
				$user = Sentry::createUser(array(
					'first_name'	=> Input::get('namaDepan'),
					'last_name' 	=> Input::get('namaBelakang'),
					'email' 		=> Input::get('email'),
					'password' 		=> Input::get('password'),
					'activated' 	=> true,
				));
	 
				$groupbyid = Sentry::findGroupById(Input::get('group'));
				$user->addGroup($groupbyid);
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
				Session::flash('error', 'Login field is required.');
				return Redirect::to('user-control/user');
			}
			catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
			{
				Session::flash('error', 'Password field is required.');
				return Redirect::to('user-control/user');
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
				Session::flash('error', 'User with this login already exists.');
				return Redirect::to('user-control/user');
			}
			catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
			{
				Session::flash('error', 'Group was not found.');
				return Redirect::to('user-control/user');
			}
		Session::flash('success', 'Data Berhasil Ditambahkan');
		return Redirect::to('user-control/user');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$userbyid = Sentry::findUserByID(Crypt::decrypt($id));
		$groupbyuser = $userbyid->getGroups();
		$groupbyuser = json_decode($groupbyuser, true);
		$group = Group::lists('name', 'id');
		$data =
		[
			'userbyid' => $userbyid,
			'group' => $group,
			'groupbyuser' => $groupbyuser[0]['id']
		];
		return View::make('backend.pages.usercontrol.user.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$rules = array(
			'namaDepan' => 'required',
			'namaBelakang' => 'required',
			'email' => 'required|email|unique:users,email,'. Crypt::decrypt($id), 
			'password' => 'required|between:4,11',
			'confirmPassword' => 'between:4,11|same:password',
			'group' => 'required',
		);
	 
		$validator = Validator::make(Input::all(), $rules);
	 
		if ($validator->fails()) {  
			return Redirect::back()->withErrors($validator)->withInput();
		} else {
			try
			{
			$user 				= Sentry::findUserById(Crypt::decrypt($id));
			$groupbyuser 		= $user->getGroups();
			$groupbyuser 		= json_decode($groupbyuser, true);
			$groupbyuser 		= Sentry::findGroupById($groupbyuser[0]['id']);
			$user->removeGroup($groupbyuser);
	 
			$groupbyuser		 = Sentry::findGroupById(Input::get('group'));
			$user->addGroup($groupbyuser);
			 
			$user->first_name	= Input::get('namaDepan');
			$user->last_name 	= Input::get('namaBelakang');
			$user->email 		= Input::get('email');
			$user->password 	= Input::get('password');
	 
			if ($user->save())
			{
				Session::flash('success', 'Data Berhasil Ditambahkan');
				return Redirect::to('user-control/user');
			}
			else
			{
				Session::flash('success', 'Data Gagal Ditambahkan');
				return Redirect::to('user-control/user');
			}
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
				Session::flash('error', 'User with this login already exists.');
				return Redirect::to('user-control/user');
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				Session::flash('error', 'User was not found.');
				return Redirect::to('user-control/user');
			}
			catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
			{
				Session::flash('error', 'Group was not found.');
				return Redirect::to('user-control/user');
			}
		}  
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		
	}

	public function delete($id){
		try
		{
			$user = Sentry::findUserById(Crypt::decrypt($id));
			$user->delete();
		 }
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			Session::flash('error', 'User was not found.');
			return Redirect::to('user-control/user');
		}
		Session::flash('success', 'Data Berhasil Dihapus');
		return Redirect::to('user-control/user');
	}
}
