<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InfoLokasi extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('info_lokasi', function(Blueprint $table) {
			$table->increments('id');
			$table->string('lokasi_kode', 50);
			$table->string('lokasi_nama', 100);
			$table->smallInteger('lokasi_propinsi');
			$table->smallInteger('lokasi_kabupatenkota');
			$table->smallInteger('lokasi_kecamatan');
			$table->smallInteger('lokasi_kelurahan');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('info_lokasi');
	}

}
