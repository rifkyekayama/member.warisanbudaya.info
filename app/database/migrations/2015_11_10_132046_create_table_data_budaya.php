<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDataBudaya extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('databudaya', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('Nama_Karya_Budaya')->nullable();
			$table->string('Kondisi_Karya_Budaya_Saat_Ini')->nullable();
			$table->smallInteger('provinsi')->nullable();
			$table->smallInteger('kabupaten')->nullable();
			$table->smallInteger('kecamatan')->nullable();
			$table->string('kelurahan', 15)->nullable();
			$table->string('pos', 10)->nullable();
			$table->text('Deskripsi_Karya_Budaya')->nullable();
			$table->string('Pelaku_Karya_Budaya')->nullable();
			$table->string('nama')->nullable();
			$table->string('alamat')->nullable();
			$table->string('telp')->nullable();
			$table->string('email')->nullable();
			$table->time('waktu')->nullable();
			$table->date('tanggal')->nullable();
			$table->string('foto_terbaru')->nullable();
			$table->text('deskripsi_foto')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('databudaya');
	}

}
