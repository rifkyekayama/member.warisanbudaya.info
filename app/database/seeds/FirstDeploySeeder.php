<?php

class FirstDeploySeeder extends Seeder {

	public function run(){
		$data_permission = [
			'budaya.read'			=> 1,
			'budaya.create' 		=> 1,
			'budaya.update'			=> 1,
			'budaya.delete' 		=> 1,

			'group.read'			=> 1,
			'group.create' 			=> 1,
			'group.update'			=> 1,
			'group.delete' 			=> 1,

			'user.read'				=> 1,
			'user.create'			=> 1,
			'user.update'			=> 1,
			'user.delete'			=> 1,
		];
		Sentry::createGroup(array(
			'name'        => 'Admin',
			'permissions' => $data_permission,
		));

		$user = Sentry::createUser(array(
			'first_name'	=> "Super",
			'last_name' 	=> "Admin",
			'email' 		=> "super@admin.com",
			'password' 		=> "admin",
			'activated' 	=> true,
		));

		$groupbyid = Sentry::findGroupById(1);
		$user->addGroup($groupbyid);
	}
}