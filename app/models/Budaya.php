<?php

class Budaya extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'databudaya';

	public static $rules = [
		// 'title' => 'required'
		'Nama_Karya_Budaya'				=> 'required|alpha_dash',
		'Kondisi_Karya_Budaya_Saat_Ini'	=> 'required',
		'provinsi'						=> 'required',
		'kabupaten'						=> 'required',
		'kecamatan'						=> 'required',
		'kelurahan'						=> 'required',
		'Deskripsi_Karya_Budaya'		=> 'required|max:500',
		'Pelaku_Karya_Budaya'			=> 'required',
		'nama'							=> 'required|alpha',
		'alamat'						=> 'required',
		'telp'							=> 'required|numeric',
		'email'							=> 'required|email',
		'waktu'							=> 'required',
		'tanggal'						=> 'required',
		'foto_terbaru'					=> 'images|mimes:jpeg,bmp,jpg.png,gif'
	];

}
