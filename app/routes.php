<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('login', 'BackendAppController@login');
Route::get('pendaftaran', 'BackendAppController@register');
Route::post('pendaftaran', 'BackendAppController@doRegister');
Route::get('aktivasi_member/{token}', 'BackendAppController@activationMember');
Route::post('auth', 'BackendAppController@auth');
Route::group(array('before' => 'sentry_auth'), function(){
	Route::get('/', 'BackendAppController@index');
	Route::get('logout', 'BackendAppController@logout');
	Route::resource('budaya', 'DataBudayaController');
	Route::group(array('prefix' => 'budaya'), function(){
		Route::get('kabupaten/{provinsi}', 'DataBudayaController@getKabupaten');
		Route::get('kecamatan/{provinsi}/{kabupaten}', 'DataBudayaController@getKecamatan');
		Route::get('kelurahan/{provinsi}/{kabupaten}/{kecamatan}', 'DataBudayaController@getKelurahan');
		Route::get('delete/{id}', 'DataBudayaController@delete');
	});
	Route::group(array('prefix' => 'user-control'), function(){
		Route::resource('user', 'UserControlController');
		Route::get('user/delete/{id}', 'UserControlController@delete');
		Route::resource('group', 'GroupControlController');
		Route::get('group/delete/{id}', 'GroupControlController@delete');
	});
});
