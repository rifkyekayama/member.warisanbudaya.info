<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{{ ApplicationHelper::avatar(Sentry::getUser()->email) }}" class="img-circle" alt="User Image" />
			</div>
			<div class="pull-left info">
				<p>{{ Sentry::getUser()->first_name." ".Sentry::getUser()->last_name }}</p>

				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search..."/>
				<span class="input-group-btn">
					<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="{{ Request::is('/') ? 'active' : '' }}">
				<a href="{{ URL::to('/') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
			</li>       
			@if(Sentry::getUser()->hasAccess('budaya.read'))
				<li class="{{ Request::is('budaya') || Request::is('budaya/*') ? 'active' : '' }}">
					<a href="{{ URL::to('budaya') }}"><i class="fa fa-dashboard"></i> <span>Data Budaya</span></a>
				</li>  
			@endif
			@if(Sentry::getUser()->hasAccess('user.read') && Sentry::getUser()->hasAccess('group.read'))    
				<li class="{{ Request::is('user-control/*') ? 'active treeview' : '' }}">
					<a href="#">
						<i class="fa fa-pie-chart"></i>
						<span>User Control</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						@if(Sentry::getUser()->hasAccess('group.read'))
							<li class="{{ Request::is('user-control/group') || Request::is('user-control/group/*') ? 'active' : '' }}"><a href="{{ URL::to('user-control/group') }}"><i class="fa fa-circle-o"></i> Group</a></li>
						@endif
						@if(Sentry::getUser()->hasAccess('user.read'))
							<li class="{{ Request::is('user-control/user') || Request::is('user-control/user/*') ? 'active' : '' }}"><a href="{{ URL::to('user-control/user') }}"><i class="fa fa-circle-o"></i> User</a></li>
						@endif
					</ul>
				</li>       
			@endif     
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>