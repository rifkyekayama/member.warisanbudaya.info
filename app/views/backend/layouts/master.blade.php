<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Admin Area | Warisan Budaya Indonesia</title>
    	
    	<link rel="shortcut icon" href="{{ asset('favicon.png') }}">

		<!-- Tell the browser to be responsive to screen width -->
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.4 -->
		{{ HTML::style('assets/backend/bootstrap/css/bootstrap.min.css') }}
		<!-- Font Awesome Icons -->
		{{ HTML::style('assets/backend/plugins/Font-Awesome-master/css/font-awesome.min.css') }}
		<!-- Ionicons -->
	{{-- 	{{ HTML::style('assets/backend/dist/css/ionicons.min.css') }} --}}
		{{ HTML::style('assets/backend/plugins/ionicons/css/ionicons.min.css') }}
		<!-- Theme style -->
		{{ HTML::style('assets/backend/dist/css/AdminLTE.min.css') }}
		<!-- AdminLTE Skins. Choose a skin from the css/skins 
				 folder instead of downloading all of them to reduce the load. -->
		{{ HTML::style('assets/backend/dist/css/skins/_all-skins.min.css') }}
		@yield('css')

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="skin-blue sidebar-mini">
		<!-- Site wrapper -->
		<div class="wrapper">
			
			@include('backend.includes._header')

			<!-- =============================================== -->

			<!-- Left side column. contains the sidebar -->
			@include('backend.includes._sidebar')

			<!-- =============================================== -->

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					@yield('content-header')
				</section>

				<!-- Main content -->
				<section class="content">

					@yield('content')

				</section><!-- /.content -->
			</div><!-- /.content-wrapper -->

			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					
				</div>
				<strong><a>Direktorat Jenderal Kebudayaan | Kementerian Pendidikan dan Kebudayaan Republik Indonesia &copy; 2015</a>.</strong>
			</footer>
			<!-- Add the sidebar's background. This div must be placed
					 immediately after the control sidebar -->
			<div class='control-sidebar-bg'></div>
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.4 -->
		{{ HTML::script('assets/backend/plugins/jQuery/jQuery-2.1.4.min.js') }}
		<!-- Bootstrap 3.3.2 JS -->
		{{ HTML::script('assets/backend/bootstrap/js/bootstrap.min.js') }}
		<!-- SlimScroll -->
		{{ HTML::script('assets/backend/plugins/slimScroll/jquery.slimscroll.min.js') }}
		<!-- FastClick -->
		{{ HTML::script('assets/backend/plugins/fastclick/fastclick.min.js') }}
		<!-- AdminLTE App -->
		{{ HTML::script('assets/backend/dist/js/app.min.js') }}
		<!-- Demo -->
		{{ HTML::script('assets/backend/dist/js/demo.js') }}
		@yield('js')
		@yield('ajax')
	</body>
</html>
