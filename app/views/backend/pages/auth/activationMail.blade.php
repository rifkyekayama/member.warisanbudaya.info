<html>
	<body>
		<p>
			Salam dan Selamat Datang {{ $nama }},
			<br><br>
			Terimakasih telah mendaftarkan diri Anda di warisanbudaya.info
			<br><br>
			Untuk mengaktivasi akun Anda, silahkan klik url berikut : 
			<br><br>
			URL : <a href="{{ URL::to('aktivasi_member'.'/'.$token) }}">{{ URL::to('aktivasi_member'.'/'.$token) }}</a>

			Hormat kami,
			Admin Warisan Indonesia
		</p>
	</body>
</html>