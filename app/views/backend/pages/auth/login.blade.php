<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Member Area | Warisan Budaya Indonesia</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link rel="shortcut icon" href="{{ asset('favicon.png') }}">
		<!-- Bootstrap 3.3.4 -->
		{{ HTML::style('assets/backend/bootstrap/css/bootstrap.min.css') }}
		<!-- Font Awesome Icons -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		{{ HTML::style('assets/backend/dist/css/AdminLTE.min.css') }}
		<!-- iCheck -->
		{{ HTML::style('assets/backend/plugins/iCheck/square/blue.css') }}

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="{{ URL::to('/') }}"><b>WarisanBudaya</b><br>Login System</a>
			</div><!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg">Sign in to start your session</p>

				@if(Session::has('error'))
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-close"></i> Error!</h4>
						{{ Session::get('error') }}
					</div>
				@endif

				@if(Session::has('success'))
					<div class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-close"></i> Sukses!</h4>
						{{ Session::get('success') }}
					</div>
				@endif

				{{ Form::open(array('url' => 'auth')) }}
					<div class="form-group has-feedback">
						{{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email')) }}
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						{{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-8">    
							<div class="checkbox icheck">
								<label>
									{{ Form::checkbox('remember','true') }} Remember Me
								</label>
							</div>                        
						</div><!-- /.col -->
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
						</div><!-- /.col -->
					</div>
				{{ Form::close() }}

				{{-- <div class="social-auth-links text-center">
					<p>- OR -</p>
					<a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
					<a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
				</div><!-- /.social-auth-links --> --}}

				<a href="#">I forgot my password</a><br>
				<a href="{{ URL::to('pendaftaran') }}" class="text-center">Register a new membership</a>

			</div><!-- /.login-box-body -->
		</div><!-- /.login-box -->

		<!-- jQuery 2.1.4 -->
		{{ HTML::script('assets/backend/plugins/jQuery/jQuery-2.1.4.min.js') }}
		<!-- Bootstrap 3.3.2 JS -->
		{{ HTML::script('assets/backend/bootstrap/js/bootstrap.min.js') }}
		<!-- iCheck -->
		{{ HTML::script('assets/backend/plugins/iCheck/icheck.min.js') }}
		<script>
			$(function () {
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					increaseArea: '20%' // optional
				});
			});
		</script>
	</body>
</html>