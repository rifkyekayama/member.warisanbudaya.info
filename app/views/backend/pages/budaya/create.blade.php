@extends('backend.layouts.master')

@section('css')
	{{ HTML::style('assets/backend/plugins/daterangepicker/daterangepicker-bs3.css') }}
	{{ HTML::style('assets/backend/plugins/iCheck/all.css') }}
	{{ HTML::style('assets/backend/plugins/colorpicker/bootstrap-colorpicker.min.css') }}
	{{ HTML::style('assets/backend/plugins/timepicker/bootstrap-timepicker.min.css') }}
	{{ HTML::style('assets/backend/plugins/iCheck/all.css') }}
	{{ HTML::style('assets/backend/plugins/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css') }}
@stop

@section('content-header')
	<h1>
		Tambah Data Budaya
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Budaya</a></li>
		<li class="active">Tambah Data Budaya</li>
	</ol>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">

			@if($errors->has())
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h4><i class="icon fa fa-close"></i> Error!</h4>
					{{ HTML::ul($errors->all()) }}
				</div>
			@endif

			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Tambah Data Budaya</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				{{ Form::open(array('url' => 'budaya', 'files'=> true)) }}
					<div class="box-body">
						<div class="form-group {{ $errors->has('Nama_Karya_Budaya') ? 'has-error' : '' }}">
							<label>1. Nama Karya Budaya <small>(Isi Nama yang paling umum dipakai)</small></label>
							{{ Form::text('Nama_Karya_Budaya', Input::old('Nama_Karya_Budaya'), array('class' => 'form-control')) }}
						</div>
						<div class="form-group {{ $errors->has('Kondisi_Karya_Budaya_Saat_Ini') ? 'has-error' : '' }}">
							<label >2. Kondisi Karya Budaya Saat Ini <small>(contreng salah satu)</small></label>
							<div class="row">
								<div class="col-md-6">
									<div class="box box-info">
										<div class="box-body">
											<div class="form-group">
												{{ Form::radio('Kondisi_Karya_Budaya_Saat_Ini', 'Sedang Berkembang', null, array('class' => 'flat-red')) }}
												<label>Sedang Berkembang.</label>
											</div>
											<div class="form-group">
												{{ Form::radio('Kondisi_Karya_Budaya_Saat_Ini', 'Masih bertahan', null, array('class' => 'flat-red')) }}
												<label>Masih bertahan.</label>
											</div>
											<div class="form-group">
												{{ Form::radio('Kondisi_Karya_Budaya_Saat_Ini', 'Sudah berkurang', null, array('class' => 'flat-red')) }}
												<label>Sudah berkurang.</label>
											</div>
											<div class="form-group">
												{{ Form::radio('Kondisi_Karya_Budaya_Saat_Ini', 'Terancam Punah', null, array('class' => 'flat-red')) }}
												<label>Terancam Punah.</label>
											</div>
											<div class="form-group">
												{{ Form::radio('Kondisi_Karya_Budaya_Saat_Ini', 'Sudah punah/ tidak berfungsi lagi dalam masyarakat', null, array('class' => 'flat-red')) }}
												<label>Sudah punah/ tidak berfungsi lagi dalam masyarakat.</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label >3. Lokasi Karya Budaya</label>
							<div class="detailLokasiKaryaBudaya">
								<div class="row">
									<div class="col-md-6">
										<div class="box box-info">
											<div class="box-body">
												<div class="form-group {{ $errors->has('provinsi') ? 'has-error' : '' }}">
													<label>Provinsi</label>
													{{ Form::select('provinsi', array('default' => 'Pilih Provinsi') + $lokasi, Input::old('provinsi') != null ? Input::old('provinsi') : 'default', array('class' => 'form-control', 'id' => 'provinsi')) }}
												</div>
												<div class="form-group {{ $errors->has('kabupaten') ? 'has-error' : '' }}">
													<label>Kabupaten</label>
													<div id="kabupatenContainer">
														{{ Form::select('kabupaten', array('default' => 'Pilih Kabupaten'), Input::old('kabupaten') != null ? Input::old('kabupaten') : 'default', array('class' => 'form-control', 'id' => 'kabupaten')) }}
													</div>
												</div>
												<div class="form-group {{ $errors->has('kecamatan') ? 'has-error' : '' }}">
													<label>Kecamatan</label>
													<div id="kecamatanContainer">
														{{ Form::select('kecamatan', array('default' => 'Pilih Kecamatan'), Input::old('kecamatan') != null ? Input::old('kecamatan') : 'default', array('class' => 'form-control', 'id' => 'kecamatan')) }}
													</div>
												</div>
												<div class="form-group {{ $errors->has('kelurahan') ? 'has-error' : '' }}">
													<label>Desa/Kelurahan</label>
													<div id="kelurahanContainer">
														{{ Form::select('kelurahan', array('default' => 'Pilih Kelurahan'), Input::old('kelurahan') != null ? Input::old('kelurahan') : 'default', array('class' => 'form-control', 'id' => 'kelurahan')) }}
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group {{ $errors->has('Deskripsi_Karya_Budaya') ? 'has-error' : '' }}">
							<label >4. Deskripsi Karya Budaya</label>
							{{ Form::textarea('Deskripsi_Karya_Budaya', Input::old('Deskripsi_Karya_Budaya'), ['size' => '30x10', 'maxlength' => '500', 'class' => 'form-control', 'id' => 'deskripsi']) }}
						</div>
						<div class="form-group {{ $errors->has('Pelaku_Karya_Budaya') ? 'has-error' : '' }}">
							<label >5. Pelaku Karya Budaya</label>
							{{ Form::text('Pelaku_Karya_Budaya', Input::old('Pelaku_Karya_Budaya'), ['class' => 'form-control']) }}
						</div>

						<div class="form-group">
							<label >6. Foto Terbaru Dengan Penjelasan</label>
							<div class="row">
								<div class="col-md-12">
									<div class="box box-default">
										<div class="box box-body">
											<div class="form-group">
												{{ Form::file('Foto_Karya_Budaya[]', ['class' => 'form-control', 'multiple' => true, 'accept' => 'image/*']) }}
											</div>
											<div class="form-group {{ $errors->has('deskripsi_foto') ? 'has-error' : '' }}">
												<small>Deskripsi Foto</small>
												{{ Form::textarea('deskripsi_foto', Input::old('Deskripsi_Karya_Budaya'), ['size' => '30x10', 'maxlength' => '500', 'class' => 'form-control', 'id' => 'deskripsi']) }}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label >7. Pelapor dan Waktu Pelaporan</label>
							<div class="row">
								<div class="col-md-6">
									<div class="box box-info">
										<div class="box-body">
											<div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
												<label>Nama</label>
												{{ Form::text('nama', Input::old('nama'), array('class' => 'form-control')) }}
											</div>
											<div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
												<label>Alamat</label>
												{{ Form::text('alamat', Input::old('alamat'), array('class' => 'form-control')) }}
											</div>
											<div class="form-group {{ $errors->has('pos') ? 'has-error' : '' }}">
												<label>Kode Pos</label>
												{{ Form::number('pos', Input::old('pos'), array('class' => 'form-control')) }}
											</div>
											<div class="form-group {{ $errors->has('telp') ? 'has-error' : '' }}">
												<label>No. Telp./No. Fax/ No. Mobile</label>
												{{ Form::number('telp', Input::old('telp'), array('class' => 'form-control')) }}
											</div>
											<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
												<label>Alamat email</label>
												{{ Form::email('email', Input::old('email'), array('class' => 'form-control')) }}
											</div>
											<div class="bootstrap-timepicker">
												<div class="form-group">
													<label>Time picker:</label>
													<div class="input-group">
														{{ Form::text('waktu', null, array('class' => 'form-control timepicker')) }}
														<div class="input-group-addon">
															<i class="fa fa-clock-o"></i>
														</div>
													</div><!-- /.input group -->
												</div><!-- /.form group -->
											</div>
											<div class="form-group">
												<label>Tanggal</label>
												{{ Form::text('tanggal', date("d F Y", time()), array('class' => 'form-control datepicker')) }}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="box box-danger">
									<div class="box-body">
										<p align="justify">
											<strong>Catatan:</strong><br>
											<ul align="justify">1. Tidak boleh mengusulkan karya budaya yang melanggar peraturan 	perundang-undangan RI.</ul>
											<ul align="justify">2. Catatan mengenai karya budaya bersifat umum dan singkat. Untuk hal-hal yang bersifat khusus, orang yang berkepentingan akan dipersilakan menghubungi komunitas/ organisasi/ asosiasi/ badan/ paguyuban, kelompok sosial, atau perseorangan penanggung jawab karya budaya atau guru budaya/maestro pemegang kekayaan intelektual atas karya budaya yang bersangkutan.</ul>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				{{ Form::close() }}
			</div><!-- /.box -->
		</div>
	</div>
@stop

@section('ajax')
	<script type="text/javascript">
		$(document).ready(function(){
			$('#provinsi').on('change', function(){
				$.ajax({
					type: "GET",
					url: "{{ URL::to('budaya/kabupaten') }}"+"/"+this.value,
					cache: false,
					success: function(data){
						$('#kabupatenContainer').html(data);
					}
				});
			});

			$('#kabupatenContainer').on('change', '#kabupaten', function(){
				$.ajax({
					type: "GET",
					url: "{{ URL::to('budaya/kecamatan') }}"+"/"+$('#provinsi').val()+"/"+this.value,
					cache: false,
					success: function(data){
						$('#kecamatanContainer').html(data);
					}
				});
			});

			$('#kecamatanContainer').on('change', '#kecamatan', function(){
				$.ajax({
					type: "GET",
					url: "{{ URL::to('budaya/kelurahan') }}"+"/"+$('#provinsi').val()+"/"+$('#kabupaten').val()+"/"+this.value,
					cache: false,
					success: function(data){
						$('#kelurahanContainer').html(data);
					}
				});
			});

			//Timepicker
			$(".timepicker").timepicker({
				showInputs: false
			});

			CKEDITOR.replace('deskripsi');

			$('.datepicker').datepicker({
				format: 'dd MM yyyy',
				startDate: '-3d'
			});

			//iCheck for checkbox and radio inputs
			$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
				checkboxClass: 'icheckbox_minimal-blue',
				radioClass: 'iradio_minimal-blue'
			});
			//Red color scheme for iCheck
			$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
				checkboxClass: 'icheckbox_minimal-red',
				radioClass: 'iradio_minimal-red'
			});
			//Flat red color scheme for iCheck
			$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
				checkboxClass: 'icheckbox_flat-green',
				radioClass: 'iradio_flat-green'
			});
		})
	</script>
@stop

@section('js')
	{{ HTML::script('assets/backend/plugins/iCheck/icheck.min.js') }}
	{{ HTML::script('assets/backend/plugins/timepicker/bootstrap-timepicker.min.js') }}
	{{ HTML::script('assets/backend/plugins/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}
	{{ HTML::script('assets/backend/plugins/ckeditor/ckeditor.js') }}
@stop