<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th width="10%">No.</th>
			<th>Tahun</th>
			<th>Nama Karya Budaya</th>
			<th width="15%">Aksi</th>
		</tr>
	</thead>
	<tbody>
		{{--*/ $no=1; /*--}}
		@foreach($budaya as $value)
			<tr>
				<td>{{ $no }}</td>
				<td>{{ date("d F Y", strtotime($value->tanggal)) }}</td>
				<td>{{ $value->Nama_Karya_Budaya }}</td>
				<td>
					{{-- <a href="#" class="btn btn-social-icon btn-success btn-sm"><i class="fa fa-search"></i></a> --}}
					<a href="{{ URL::to('budaya/'.Crypt::encrypt($value->id).'/edit') }}" class="btn btn-social-icon btn-primary btn-sm"><i class="fa fa-edit"></i></a>
					<a href="{{ URL::to('budaya/delete/'.Crypt::encrypt($value->id)) }}" id="{{ Crypt::encrypt($value->id) }}" class="btn btn-social-icon btn-danger btn-sm"><i class="fa fa-trash"></i></a>
				</td>
				{{--*/ $no++; /*--}}
			</tr>
		@endforeach
	</tbody>
</table>