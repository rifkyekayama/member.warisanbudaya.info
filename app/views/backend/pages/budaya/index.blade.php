@extends('backend.layouts.master')

@section('css')
	<!-- DATA TABLES -->
	{{ HTML::style('assets/backend/plugins/datatables/dataTables.bootstrap.css') }}
@stop

@section('content-header')
	<h1>
		Data Budaya Indonesia
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Budaya</li>
	</ol>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			@if(Session::has('success'))
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h4>	<i class="icon fa fa-check"></i> Success!</h4>
					{{ Session::get('success') }}
				</div>
			@endif
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Daftar Data Budaya</h3>
					<div class="pull-right">
						<a href="{{ URL::to('budaya/create') }}"><button class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Tambah Data Budaya</button></a>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body" id="tableBudayaContainer">
					@include('backend.pages.budaya.element._tableBudaya')
				</div>
			</div><!-- /.box -->
		</div>
	</div>
@stop

@section('js')
	<!-- DATA TABES SCRIPT -->
	{{ HTML::script('assets/backend/plugins/datatables/jquery.dataTables.min.js') }}
	{{ HTML::script('assets/backend/plugins/datatables/dataTables.bootstrap.min.js') }}
	<!-- page script -->
	<script type="text/javascript">
	  $(function () {
		$("#example1").dataTable();
		$('#example2').dataTable({
		  "bPaginate": true,
		  "bLengthChange": false,
		  "bFilter": false,
		  "bSort": true,
		  "bInfo": true,
		  "bAutoWidth": false
		});
	  });
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.deleteBudaya').live('click', function(){
				$.ajax({
					type: "DELETE",
					url: "{{ URL::to('budaya') }}"+"/"+this.id,
					cache: false,
					success: function(data){
						$('#tableBudayaContainer').html(data);
						$("#example1").dataTable();
						$('#example2').dataTable({
						  "bPaginate": true,
						  "bLengthChange": false,
						  "bFilter": false,
						  "bSort": true,
						  "bInfo": true,
						  "bAutoWidth": false
						});
					}
				})
			});
		});
	</script>
@stop