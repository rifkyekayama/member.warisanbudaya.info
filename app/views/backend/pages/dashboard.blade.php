@extends('backend.layouts.master')

@section('content-header')
	<h1>
		<center>Warisan Budaya Indonesia</center>
		
	</h1>
@stop

@section('content')
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>{{ $jumlahBudaya }}</h3>
				<p>Data Budaya</p>
			</div>
			<div class="icon">
				<i class="ion ion-android-document"></i>
			</div>
			<a href="{{ URL::to('budaya') }}" class="small-box-footer">Lihat lebih lanjut <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div><!-- ./col -->

@stop