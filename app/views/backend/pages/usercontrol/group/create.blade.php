@extends('backend.layouts.master')

@section('css')
	<!-- iCheck -->
	{{ HTML::style('assets/backend/plugins/iCheck/square/blue.css') }}
@stop

@section('content-header')
	<h1>
		Group Control
		<small>warisanbudaya.info</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">User Control</a></li>
		<li class="active">Group</li>
	</ol>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Tambah Group User</h3>
				</div><!-- /.box-header -->
				{{ Form::open(array('url' => 'user-control/group')) }}
					<div class="box-body">
						<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							<label>Nama Group</label>
							{{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Budaya: </label>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[budaya.read]','1') }} Lihat data budaya
										</label>
									</div>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[budaya.create]','1') }} Tambah Data Budaya
										</label>
									</div>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[budaya.update]','1') }} Edit Data Budaya
										</label>
									</div>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[budaya.delete]','1') }} Delete Data Budaya
										</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Group Control: </label>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[group.read]','1') }} Lihat Group
										</label>
									</div>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[group.create]','1') }} Tambah Group
										</label>
									</div>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[group.update]','1') }} Edit Group
										</label>
									</div>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[group.delete]','1') }} Delete Group
										</label>
									</div>
								</div>
								<div class="form-group">
									<label>User Control: </label>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[user.read]','1') }} Lihat User
										</label>
									</div>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[user.create]','1') }} Tambah User
										</label>
									</div>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[user.update]','1') }} Edit User
										</label>
									</div>
									<div class="checkbox icheck">
										<label>
											{{ Form::checkbox('cb[user.delete]','1') }} Delete User
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop

@section('js')
	<!-- iCheck -->
	{{ HTML::script('assets/backend/plugins/iCheck/icheck.min.js') }}
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});
		});
	</script>
@stop