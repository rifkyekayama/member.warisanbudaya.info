@extends('backend.layouts.master')

@section('css')
	<!-- DATA TABLES -->
	{{ HTML::style('assets/backend/plugins/datatables/dataTables.bootstrap.css') }}
@stop

@section('content-header')
	<h1>
		Group Control
	</h1>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			@if(Session::has('success'))
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h4>	<i class="icon fa fa-check"></i> Success!</h4>
					{{ Session::get('success') }}
				</div>
			@endif
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Daftar Group (Hak Akses)</h3>
					@if(Sentry::getUser()->hasAccess('group.create'))
						<div class="pull-right">
							<a href="{{ URL::to('user-control/group/create') }}"><button class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Tambah Group</button></a>
						</div>
					@endif
				</div><!-- /.box-header -->
				<div class="box-body" id="tableBudayaContainer">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="10%">No.</th>
								<th>Nama Grup</th>
								<th width="15%">Aksi</th>
							</tr>
						</thead>
						<tbody>
							{{--*/ $no=1; /*--}}
							@foreach($group as $value)
								<tr>
									<td>{{ $no }}</td>
									<td>{{ $value->name }}</td>
									<td>
										<div class="margin">
											<div class="btn-group">
												<button type="button" class="btn btn-success btn-flat">Aksi</button>
												<button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
													<span class="caret"></span>
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												@if(Sentry::getUser()->hasAccess('group.update') && Sentry::getUser()->hasAccess('group.delete'))
													<ul class="dropdown-menu" role="menu">
														@if(Sentry::getUser()->hasAccess('group.update'))
															<li><a href="{{ URL::to('user-control/group/'.Crypt::encrypt($value->id).'/edit') }}">Edit</a></li>
														@endif
														<li class="divider"></li>
														@if(Sentry::getUser()->hasAccess('group.delete'))
															<li><a href="{{ URL::to('user-control/group/delete/'.Crypt::encrypt($value->id)) }}" id="{{ Crypt::encrypt($value->id) }}">Delete</a></li>
														@endif
													</ul>
												@endif
											</div>
										</div>
									</td>
									{{--*/ $no++; /*--}}
								</tr>
							@endforeach
						</tbody>
						{{-- <tfoot>
							<tr>
								<th>Rendering engine</th>
								<th>Browser</th>
								<th>Platform(s)</th>
								<th>Engine version</th>
							</tr>
						</tfoot> --}}
					</table>
				</div>
			</div><!-- /.box -->
		</div>
	</div>
@stop

@section('js')
	<!-- DATA TABES SCRIPT -->
	{{ HTML::script('assets/backend/plugins/datatables/jquery.dataTables.min.js') }}
	{{ HTML::script('assets/backend/plugins/datatables/dataTables.bootstrap.min.js') }}
	<!-- page script -->
	<script type="text/javascript">
	  $(function () {
		$("#example1").dataTable();
		$('#example2').dataTable({
		  "bPaginate": true,
		  "bLengthChange": false,
		  "bFilter": false,
		  "bSort": true,
		  "bInfo": true,
		  "bAutoWidth": false
		});
	  });
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.deleteBudaya').live('click', function(){
				$.ajax({
					type: "DELETE",
					url: "{{ URL::to('budaya') }}"+"/"+this.id,
					cache: false,
					success: function(data){
						$('#tableBudayaContainer').html(data);
						$("#example1").dataTable();
						$('#example2').dataTable({
						  "bPaginate": true,
						  "bLengthChange": false,
						  "bFilter": false,
						  "bSort": true,
						  "bInfo": true,
						  "bAutoWidth": false
						});
					}
				})
			});
		});
	</script>
@stop