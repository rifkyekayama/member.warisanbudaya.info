@extends('backend.layouts.master')

@section('content-header')
	<h1>
		Tambah User
		<small>warisanbudaya.info</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Examples</a></li>
		<li class="active">Blank page</li>
	</ol>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Tambah Elemen Budaya</h3>
				</div><!-- /.box-header -->
				{{ Form::open(array('url' => 'user-control/user', 'data-parsley-validate')) }}
					<div class="box-body">
						<div class="form-group">
							<label>Email</label>
							{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'required')) }}
						</div>
						<div class="form-group">
							<label>Password:</label>
							{{ Form::password('password', array('class' => 'form-control', 'required')) }}
						</div>
						<div class="form-group">
							<label>Confirm Password:</label>
							{{ Form::password('confirmPassword', array('class' => 'form-control', 'required')) }}
						</div>
						<div class="form-group">
							<label>Nama Depan:</label>
							{{ Form::text('namaDepan', Input::old('namaDepan'), array('class' => 'form-control', 'required')) }}
						</div>
						<div class="form-group">
							<label>Nama Belakang:</label>
							{{ Form::text('namaBelakang', Input::old('namaBelakang'), array('class' => 'form-control', 'required')) }}
						</div>
						<div class="form-group">
							<label>Group:</label>
							{{ Form::select('group', $group, null, array('class' => 'form-control')) }}
						</div>
						<div class="form-group">
							{{ Form::captcha(array('required')) }}
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
@stop

@section('js')
	{{ HTML::script('assets/backend/plugins/parsleyjs/parsley.min.js') }}
@stop