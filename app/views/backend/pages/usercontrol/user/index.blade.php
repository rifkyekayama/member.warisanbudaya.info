@extends('backend.layouts.master')

@section('content-header')
	<h1>
		User Control
	</h1>

@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			@if(Session::has('success'))
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h4>	<i class="icon fa fa-check"></i> Success!</h4>
					{{ Session::get('success') }}
				</div>
			@endif
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Daftar User</h3>
					@if(Sentry::getUser()->hasAccess('user.create'))
						<div class="pull-right">
							<a href="{{ URL::to('user-control/user/create') }}"><button class="btn btn-block btn-primary"><i class="fa fa-plus"></i> Tambah User</button></a>
						</div>
					@endif
				</div><!-- /.box-header -->
				<div class="box-body" id="tableUserContainer">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="10%">No.</th>
								<th>Nama User</th>
								<th>Group</th>
								<th width="15%">Aksi</th>
							</tr>
						</thead>
						<tbody>
							{{--*/ $no=1; /*--}}
							@foreach($user as $value)
								<tr>
									<td>{{ $no }}</td>
									<td>{{ $value->first_name.' '.$value->last_name }}</td>
									<td>{{ $value->group->group->name }}</td>
									<td>
										<div class="margin">
											<div class="btn-group">
												<button type="button" class="btn btn-success btn-flat">Aksi</button>
												<button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">
													<span class="caret"></span>
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												@if(Sentry::getUser()->hasAccess('user.update') && Sentry::getUser()->hasAccess('user.delete'))
													<ul class="dropdown-menu" role="menu">
														@if(Sentry::getUser()->hasAccess('user.update'))
															<li><a href="{{ URL::to('user-control/user/'.Crypt::encrypt($value->id).'/edit') }}">Edit</a></li>
														@endif
														<li class="divider"></li>
														@if(Sentry::getUser()->hasAccess('user.delete'))
															<li><a href="{{ URL::to('user-control/user/delete/'.Crypt::encrypt($value->id)) }}" id="{{ Crypt::encrypt($value->id) }}">Delete</a></li>
														@endif
													</ul>
												@endif
											</div>
										</div>
									</td>
									{{--*/ $no++; /*--}}
								</tr>
							@endforeach
						</tbody>
						{{-- <tfoot>
							<tr>
								<th>Rendering engine</th>
								<th>Browser</th>
								<th>Platform(s)</th>
								<th>Engine version</th>
							</tr>
						</tfoot> --}}
					</table>
				</div>
			</div><!-- /.box -->
		</div>
	</div>
@stop